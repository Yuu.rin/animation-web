# Animation web
Animation pour notre projet web comportant un **`Loader`** pour charger la cartographie, un **`Splashscreen`** lancé à l'ouverture de l'application ainsi qu'un **`Menu`** animé présent sur l'application.

Nous n'avons malheureusement pas eu le temps de réaliser toute les animations que nous voulions, l'intégration de notre site vitrine est actuellement en cours et certaines animations devait être présente sur celui-ci. 

### Membres du groupe
* Jessy Cauquy
* Alice Gautier
* Julie Dubois
* Cyril Dorchies